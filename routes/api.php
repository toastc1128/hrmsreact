<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resource('departments-api', 'DepartmentApiController');
Route::get('departmentstaff-api', 'DepartmentStaffApiController@index')
        ->name('departmentstaff-api.index');
Route::get('departmentstaff-api/managers', 'DepartmentStaffApiController@managers')
        ->name('departmentstaff-api.managers');
