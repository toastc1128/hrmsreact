<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class DepartmentStaff extends Model
{
    protected $table = 'department_staff';

    protected $fillable = [
        'departmentId', 'staffId', 'isManager'
    ];

    public function staff()
    {
        return $this->belongsTo('App\Entities\Staff', 'staffId');
    }

    public function department()
    {
        return $this->belongsTo('App\Entities\Department', 'departmentId');
    }
}
