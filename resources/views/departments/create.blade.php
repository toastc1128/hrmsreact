@extends('layouts.web')

@section('web-header')
<div>Add Department</div>
@endsection

@section('web-content')
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div><br />
@endif
<form method="post" action="{{ route('departments.store') }}">
    <div class="form-group">
        @csrf
        <label for="name">Department Name:</label>
        <input type="text" class="form-control" name="name" required/>
    </div>
    <div class="form-group">
        <label for="price">Department Description:</label>
        <input type="text" class="form-control" name="description" required/>
    </div>
    <button type="submit" class="btn btn-primary">Add</button>
</form>
@endsection
