@extends('layouts.web')

@section('web-header')
<div>Edit Staff</div>
@endsection

@section('web-content')
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div><br />
    @endif
    <form method="post" action="{{ route('staffs.update', $staff->id) }}">
        @method('PATCH')
        @csrf
        <input type="hidden" class="form-control" name="departmentstaff_id" value="{{ $departmentstaff->id }}" />
        <div class="form-group">
            <label for="name">Staff Name:</label>
            <input type="text" class="form-control" name="name" value="{{ $staff->name }}" />
        </div>
        <div class="form-group">
            <label for="email">Staff Email:</label>
            <input type="text" class="form-control" name="email" value="{{ $staff->email }}" />
        </div>
        <div class="form-group">
            <label for="employment">Date of employment:</label>
            <input type="date" class="form-control" name="employment_date" value={{ $staff->employment_date }} />
        </div>
        <div class="form-group">
            <label for="department">Department:</label>
            <select name="department_id" class="form-control">
            @foreach($departments as $department)
                <option value="{{$department->id}}" @if ($departmentstaff->departmentId === $department->id) selected="selected" @endif>{{$department->name}}</option>
            @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="manager">Manager?</label>
            <select name="is_manager"  class="form-control">
                <option value="y" @if ($departmentstaff->isManager === 1) selected="selected" @endif>Yes</option>
                <option value="n" @if ($departmentstaff->isManager === 0) selected="selected" @endif>No</option>
            </select>
        </div>
        <button type="submit" class="btn btn-primary">Update</button>
    </form>
@endsection