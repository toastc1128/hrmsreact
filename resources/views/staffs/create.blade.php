@extends('layouts.web')

@section('web-header')
<div>Add Staff</div>
@endsection

@section('web-content')
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div><br />
@endif
<form method="post" action="{{ route('staffs.store') }}">
    <div class="form-group">
        @csrf
        <label for="name">Staff Name:</label>
        <input type="text" class="form-control" name="name" required/>
    </div>
    <div class="form-group">
        <label for="price">Staff Email:</label>
        <input type="text" class="form-control" name="email" required/>
    </div>
    <div class="form-group">
        <label for="quantity">Date of employment:</label>
        <input type="date" class="form-control" name="employment_date" required/>
    </div>
    <div class="form-group">
            <label for="department">Department:</label>
            <select name="department_id" class="form-control">
            @foreach($departments as $department)
                <option value="{{$department->id}}">{{$department->name}}</option>
            @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="manager">Manager?</label>
            <select name="is_manager"  class="form-control">
                <option value="y">Yes</option>
                <option value="n">No</option>
            </select>
        </div>
    <button type="submit" class="btn btn-primary">Add</button>
</form>
@endsection
