import React, { Component } from 'react';
import MyGlobleSetting from '../../MyGlobleSetting';

export default class TableRow extends Component {
    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleSubmit(event) {
      event.preventDefault();
      let apiUrl = MyGlobleSetting.apiUrl + `departments-api/${this.props.obj.id}`;
      axios.delete(apiUrl)
    }
    showEdit(id){
        if ( id !=1 ){
            return  <td>
                        <a href={MyGlobleSetting.webUrl+"departments/"+id+"/edit"} className="btn btn-primary">Edit</a>
                    </td>;
                        
        } else {
            return <td></td>;
        }
    }

    showDelete(id){
        if ( id !=1 ){
            return  <td>
                        <form onSubmit={this.handleSubmit}>
                            <input type="submit" value="Delete" className="btn btn-danger"/>        
                        </form>
                    </td>;
        } else {
            return <td></td>;
        }
    }
    render() {
      return (
          <tr>
            <td>
                {this.props.obj.id}
            </td>
            <td>
                {this.props.obj.name}
            </td>
            <td>
                {this.props.obj.description}
            </td>
            {this.showEdit(this.props.obj.id)}
            {this.showDelete(this.props.obj.id)}
          </tr>
      );
    }
}
