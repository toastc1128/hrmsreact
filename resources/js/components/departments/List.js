import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import axios from 'axios';
import TableRow from './TableRow';
import MyGlobleSetting from '../../MyGlobleSetting';
   
export default class DepartmentList extends Component {

    constructor(props) {
        super(props);
        this.state = {value: '', departments: ''};
    }
    componentDidMount(){
        axios.get(MyGlobleSetting.apiUrl + 'departments-api')
        .then((response) => {
            this.setState({ departments: response.data });
        })
        .catch(function (error) {
            console.log(error);
        })
    }

    handleChange (event) {
        axios.get(MyGlobleSetting.apiUrl + 'departments-api?name='+ event.target.value )
        .then((response) => {
            this.setState({ departments: response.data });
        }).catch(function (error) {
            console.log(error);
        })
    }

    tableRow(){
        if(this.state.departments instanceof Array){
            return this.state.departments.map(function(object, i){
                return <TableRow obj={object} key={i} />;
            })
        }
    }

    render() {
        return (
            <div>
                <label>
                    Department Name:
                    <input type="text" name="name" onChange={this.handleChange.bind(this)} />
                </label>
                <table className="table table-hover">
                    <thead>
                        <tr>
                            <td>ID</td>
                            <td>Department Name</td>
                            <td>Department Description</td>
                            <td colSpan="2">Actions</td>
                        </tr>
                    </thead>
                    <tbody>
                        {this.tableRow()}
                    </tbody>
                </table>
            </div>
        );
    }
}

if (document.getElementById('department-list')) {
    ReactDOM.render(<DepartmentList />, document.getElementById('department-list'));
}
