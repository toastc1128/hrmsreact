<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Entities\Staff;
use App\Entities\Department;
use App\Entities\DepartmentStaff;

class StaffController extends Controller
{
    // Login required.
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $departmentStaffs = DepartmentStaff::with('staff', 'department')->get();
        return view('staffs.index', compact('departmentStaffs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departments = Department::all();
        return view('staffs.create', compact('departments'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:50',
            'email' => 'required|email|max:50',
            'employment_date' => 'required|date',
            'department_id' => 'required',
            'is_manager' => 'required'
        ]);

        $staff = new Staff([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'employment_date' => $request->get('employment_date')
        ]);
        $staff->save();

        $departmentstaff = new DepartmentStaff([
            'staffId' => $staff->id,
            'departmentId' => $request->get('department_id'),
            'isManager' => $this->requestBoolVal($request->get('is_manager'))
        ]);
        $departmentstaff->save();

        return redirect('/staffs')->with('success', 'staff saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data =  array();
        $staff = Staff::find($id);
        $departments = Department::all();
        $departmentstaff = DepartmentStaff::where('staffId', $staff->id)->first();

        return view('staffs.edit', compact('staff', 'departments', 'departmentstaff'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|string|max:50',
            'email' => 'required|email|max:50',
            'employment_date' => 'required|date',
            'departmentstaff_id' => 'required',
            'department_id' => 'required',
            'is_manager' => 'required'
          ]);
    
        $staff = Staff::find($id);
        $staff->name = $request->get('name');
        $staff->email = $request->get('email');
        $staff->employment_date = $request->get('employment_date');
        $staff->save();

        $departmentstaff = DepartmentStaff::find($request->get('departmentstaff_id'));
        $departmentstaff->staffId = $id;
        $departmentstaff->departmentId = $request->get('department_id');
        $departmentstaff->isManager = $this->requestBoolVal($request->get('is_manager'));
        $departmentstaff->save();

        return redirect('/staffs')->with('success', 'Staff has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $staff = Staff::find($id);
        $staff->delete();
        DepartmentStaff::where('staffId', $id)->delete();
        return redirect('/staffs')->with('success', 'Staff has been deleted Successfully');
    }

    private function requestBoolVal($val)
    {
        if ($val == 'y') {
            $val = 1;
        } else {
            $val = 0;
        }
        return $val;
    }
}
