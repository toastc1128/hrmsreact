import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import axios from 'axios';
import TableRow from './TableRow';
import MyGlobleSetting from '../../MyGlobleSetting';

export default class DepartmentStaffList extends Component {
    constructor(props) {
        super(props);
        this.state = {department_staffs:'', filters:[]};
    }

    componentDidMount(){
        axios.get(MyGlobleSetting.apiUrl + 'departmentstaff-api')
        .then((response) => {
            this.setState({ department_staffs: response.data });
        })
        .catch(function (error) {
            console.log(error);
        })
    }

    handleChange (event) {       
        var fname = event.target.getAttribute('name');
        var fvalue = event.target.value;

        var loc_filters = this.state.filters;
        loc_filters[fname] = fvalue;
        //console.log(loc_filters);

        var filters_arr = [];
        for (var filters_key in loc_filters) {
              filters_arr.push(filters_key+'='+loc_filters[filters_key]);
        }
        var filters_text = filters_arr.join('&');
        //console.log(filters_text);

        this.setState({ filters: loc_filters });
        //console.log('state');
        //console.log(this.state.filters);
        
        axios.get(MyGlobleSetting.apiUrl + 'departmentstaff-api?' + filters_text )
        .then((response) => {
            this.setState({ department_staffs: response.data });
        }).catch(function (error) {
            console.log(error);
        })
    }

    tableRow(){
        if(this.state.department_staffs instanceof Array){
            return this.state.department_staffs.map(function(object, i){
                return <TableRow obj={object} key={i} />;
            })
        }
    }

    render() {
        return (
            <div>
                <table className="table table-hover">
                    <tbody>
                        <tr>
                            <td>
                                <div>
                                    <label>Department Name:</label>
                                    <input type="text" name="f_department_name" onChange={this.handleChange.bind(this)} />
                                </div>
                            </td>
                            <td>
                                <div>
                                    <label>Staff Name:</label>
                                    <input type="text" name="f_staff_name" onChange={this.handleChange.bind(this)} />
                                </div>
                            </td>
                            <td>
                                <div>
                                    <label>Role:</label>
                                    <select name="f_is_manager" onChange={this.handleChange.bind(this)}>
                                        <option value="all">all</option>
                                        <option value="y">Manager</option>
                                        <option value="n">Staff</option>
                                    </select>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div>
                                    <label>Employment Date From:</label>
                                    <input type="date" name="f_employment_date_from" onChange={this.handleChange.bind(this)} />
                                </div>
                            </td>
                            <td>
                                <div>
                                    <label>Employment Date To:</label>
                                    <input type="date" name="f_employment_date_to" onChange={this.handleChange.bind(this)} />
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table className="table table-hover">
                    <thead>
                        <tr>
                            <td>Department Name</td>
                            <td>Staff Name</td>
                            <td>Date of employment</td>
                            <td>Role</td>
                            <td>Manager</td>
                        </tr>
                    </thead>
                    <tbody>
                        {this.tableRow()}
                    </tbody>
                </table>
            </div>
        );
    }
}

if (document.getElementById('departmentstafflist')) {
    ReactDOM.render(<DepartmentStaffList />, document.getElementById('departmentstafflist'));
}
