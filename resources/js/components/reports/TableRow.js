import React, { Component } from 'react';
import MyGlobleSetting from '../../MyGlobleSetting';

export default class TableRow extends Component {
    constructor(props) {
        super(props);
        this.state = {managers:''};
    }
    componentDidMount(){
        axios.get(MyGlobleSetting.apiUrl + 'departmentstaff-api/managers')
        .then((response) => {
            this.setState({ managers: response.data });
        })
        .catch(function (error) {
            console.log(error);
        })
    }

    hasManager(id, name){
        var managers_arr = [];
        var loc_managers = this.state.managers;
        for (var loc_managers of loc_managers){
            //console.log(loc_managers);
            if (id == loc_managers.departmentId 
                    && name != loc_managers.staff_name){
                managers_arr.push(loc_managers.staff_name);
            }
        }

        var managers_text = managers_arr.join('|');
        //console.log(managers_text);

        return <p>{managers_text}</p>;
    }

    render() {
      return (
          <tr>
            <td>
                {this.props.obj.department_name}
            </td>
            <td>
                {this.props.obj.staff_name}
            </td>
            <td>
                {this.props.obj.employment_date}
            </td>
            <td>
                {this.props.obj.isManager == 1 ? <p className="text-success">Manager</p> : <p className="text-primary">Staff</p>}
            </td>
            <td>
                {this.hasManager(this.props.obj.departmentId, this.props.obj.staff_name)}
            </td>
          </tr>
      );
    }
}
