<?php

namespace Tests\Feature\Entities;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Entities\DepartmentStaff;

class DepartmentStaffTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testStaff()
    {
        $departmentStaff = new DepartmentStaff;
        $object = $departmentStaff->staff();
        $this->assertIsObject($object);
    }
    
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testDepartment()
    {
        $departmentStaff = new DepartmentStaff;
        $object = $departmentStaff->department();
        $this->assertIsObject($object);
    }
}
