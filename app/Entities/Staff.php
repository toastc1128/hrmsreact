<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    protected $table = 'staff';

    protected $fillable = [
        'name', 'email', 'employment_date'
    ];
}
