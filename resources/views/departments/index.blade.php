@extends('layouts.webReact')

@section('web-header')
<div>Department List</div>
@endsection

@section('web-content')
<body>
    <div id="department-list"></div>
    <script src="{{ asset('js/app.js') }}"></script>
</body>
@endsection
