<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Entities\DepartmentStaff;

class DepartmentStaffApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $queries = [];

        if (!empty($this->stringFiliter('d.name', request('f_department_name')))) {
            $queries[] = $this->stringFiliter('d.name', request('f_department_name'));
        }

        if (!empty($this->stringFiliter('s.name', request('f_staff_name')))) {
            $queries[] = $this->stringFiliter('s.name', request('f_staff_name'));
        }

        if (!is_null(request('f_is_manager')) && request('f_is_manager') != 'all') {
            $f_manager = 0;
            if (request('f_is_manager') == 'y') {
                $f_manager = 1;
            }
            $queries[] = ['isManager', '=', $f_manager];
        }

        if (!is_null(request('f_employment_date_from'))) {
            $queries[] = ['s.employment_date', '>=', request('f_employment_date_from')];
        }

        if (!is_null(request('f_employment_date_to'))) {
            $queries[] = ['s.employment_date', '<=', request('f_employment_date_to')];
        }

        $departmentStaffs = DepartmentStaff::join('staff as s', 'department_staff.staffId', '=', 's.id')
        ->join('departments as d', 'department_staff.departmentId', '=', 'd.id')
        ->where($queries)
        ->select('*', 's.*', 'd.*', 's.name as staff_name', 'd.name as department_name')
        ->orderBy('staffId')
        ->get();
        
        return response()->json($departmentStaffs);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function managers()
    {
        $queries[] = ['isManager', '=', 1];
        $fields = [
            '*',
            's.*',
            'd.*',
            's.name as staff_name',
            'd.name as department_name',
        ];

        $departmentStaffs = DepartmentStaff::join('staff as s', 'department_staff.staffId', '=', 's.id')
        ->join('departments as d', 'department_staff.departmentId', '=', 'd.id')
        ->where($queries)
        ->select($fields)
        ->orderBy('staffId')
        ->get();
        
        return response()->json($departmentStaffs);
    }

    // Need to create another class for filiters.
    public function stringFiliter($key, $value)
    {
        $arr = [];
        if (!is_null($value) && !empty($value)) {
            return [$key, 'like', '%'.$value.'%'];
        }
        return $arr;
    }
}
