<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Entities\Department;
use App\Entities\DepartmentStaff;

class DepartmentApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = new Department;

        $queries = [];
        $columns = [ 'name' ];

        
        foreach ($columns as $column) {
            if (request()->has($column)) {
                $queries[] = [$column, 'like', '%'.request($column).'%'];
            }
        }

        // if (request()->has('sort')) {
        //     $data = $data->orderBy('name', request('sort'));
        //     $queries['sort'] = request('sort');
        // }

        $data = $data->where($queries)->get();

        return response()->json($data);
    }
        

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = new Department([
            'name' => $request->get('name'),
            'description' => $request->get('description')
        ]);
        $data->save();

        return response()->json('add successfully');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new Department([
            'name' => $request->get('name'),
            'description' => $request->get('description')
        ]);
        $data->save();

        return response()->json('add successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Department::find($id);
        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Department::find($id);
        $data->name = $request->get('name');
        $data->description = $request->get('description');
        $data->save();

        return response()->json('update successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($id != 1) {
            $data = Department::find($id);
            $data->delete();

            DepartmentStaff::where('departmentId', $id)->update(['departmentId' => '1']);

            return response()->json('delete successfully');
        } else {
            return response()->json('cannot delete default department');
        }
    }
}
