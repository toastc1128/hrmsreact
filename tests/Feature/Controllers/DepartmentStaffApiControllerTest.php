<?php

namespace Tests\Feature\Controllers;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Http\Controllers\DepartmentStaffApiController;

class DepartmentStaffApiControllerTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testStringFiliter()
    {
        $departmentStaffApiController = new DepartmentStaffApiController();

        //test has value
        $result = $departmentStaffApiController->stringFiliter('name', 'value');
        $this->assertEquals(['name', 'like', '%value%'], $result);

        // test empty string
        $result = $departmentStaffApiController->stringFiliter('a', '');
        $this->assertEquals([], $result);
    }
}
