@extends('layouts.web')

@section('web-header')
<div>Staff List</div>
@endsection

@section('web-content')
@if(session()->get('success'))
    <div class="alert alert-success">
        {{ session()->get('success') }}  
    </div><br />
@endif
<table class="table table-striped">
    <thead>
        <tr>
        <td>ID</td>
        <td>Name</td>
        <td>Email</td>
        <td>Date of employment</td>
        <td>Department Name</td>
        <td>Is Manager</td>
        <td colspan="2">Action</td>
        </tr>
    </thead>
    <tbody>
    @foreach($departmentStaffs as $departmentStaff)
        <tr>
            <td>{{$departmentStaff->staff->id}}</td>
            <td>{{$departmentStaff->staff->name}}</td>
            <td>{{$departmentStaff->staff->email}}</td>
            <td>{{$departmentStaff->staff->employment_date}}</td>
            <td>{{$departmentStaff->department->name}}</td>
            <td>
            @if ($departmentStaff->isManager === 1)
                <p class="text-success">Yes</p>
            @else
                <p class="text-danger">No</p>
            @endif
            </td>
            <td><a href="{{ route('staffs.edit',$departmentStaff->staff->id)}}" class="btn btn-primary">Edit</a></td>
            <td>
                <form action="{{ route('staffs.destroy', $departmentStaff->staff->id)}}" method="post">
                @csrf
                @method('DELETE')
                <button class="btn btn-danger" type="submit">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
<div>
@endsection
