@extends('layouts.web')

@section('web-header')
<div>Department Edit</div>
@endsection

@section('web-content')
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div><br />
    @endif
    <form method="post" action="{{ route('departments.update', $department->id) }}">
        @method('PATCH')
        @csrf
        <div class="form-group">
        <label for="name">Department Name:</label>
        <input type="text" class="form-control" name="name" value="{{ $department->name }}" />
        </div>
        <div class="form-group">
        <label for="price">Department Description:</label>
        <input type="text" class="form-control" name="description" value="{{ $department->description }}" />
        </div>
        <button type="submit" class="btn btn-primary">Update</button>
    </form>
@endsection