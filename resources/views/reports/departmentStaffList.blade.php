@extends('layouts.webReact')

@section('web-header')
<div>Department Staff List</div>
@endsection

@section('web-content')
<body>
    <div id="departmentstafflist"></div>
    <script src="{{ asset('js/app.js') }}"></script>
</body>
@endsection
