@extends('layouts.app')

@section('app-content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card uper">
                    <div class="card-header">
                        @yield('web-header')
                    </div>
                    <div class="card-body">
                        @yield('web-content')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
