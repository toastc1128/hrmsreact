<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Entities\Department;

class DepartmentController extends Controller
{
    // Login required.
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('departments.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('departments.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:50',
            'description' => 'required|string|max:50'
        ]);

        $department = new Department([
            'name' => $request->get('name'),
            'description' => $request->get('description')
        ]);

        $department->save();

        return redirect('/departments')->with('success', 'Department saved!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $department = Department::find($id);
        return view('departments.edit', compact('department'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|string|max:50',
            'description' => 'required|string|max:50'
        ]);

        $department = new Department([
            'name' => $request->get('name'),
            'description' => $request->get('description')
        ]);

        $department = Department::find($id);
        $department->name = $request->get('name');
        $department->description = $request->get('description');

        $department->save();

        return redirect('/departments')->with('department', 'Department has been updated');
    }
}
